# PACE 2024

This is the solution of the GOAT team for the exact track of the PACE 2024 challenge.
The source code is open source under the MIT License.

## Brief description

Two heuristic solvers (median and barycenter) together with lower bound. If it matches the lower bound, output the solution, otherwise throw runtime error.

## Instalation

* Modern version of GCC with C++20 support is required.

* CMake version >= 3.18

* Source code is in the `code` folder. Check the `code/README.md` for installation steps.


## Solver PDF

* TBA

