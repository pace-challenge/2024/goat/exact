#include "dag.hpp"
#include "../utilities.hpp"
#include <cassert>
#include <queue>

using namespace std;

vector<pair<vertex_t, vertex_t>> DAG::commit_edge(vertex_t u, vertex_t v){
    vector<pair<vertex_t, vertex_t>> edges = get_transitive_edges_if_add(u,v);
    for(const auto & [p,s] : edges){
        add_edge(p,s);
    }
    return edges;
}

vector<pair<vertex_t, vertex_t>> DAG::get_transitive_edges_if_add(vertex_t u, vertex_t v) const {
    vector<pair<vertex_t, vertex_t>> edges;
    for(vertex_t pu : pred_.at(u)){
        for(vertex_t sv : succ_.at(v)){
            if(!succ_.at(u).count(v)){
                edges.emplace_back(pu, sv);
            }
        }
        edges.emplace_back(pu,v);
    }
    for(vertex_t sv : succ_.at(v)){
        edges.emplace_back(u,sv);
    }
    edges.emplace_back(u,v);
    return edges;
}


// void add_transitive_edges(vertex_t v, std::vector<std::pair<vertex_t, vertex_t>> & edges){
//     queue<vertex_t> q;
//     q.push(v);
//     while(!q.empty()){
//         vertex_t current = q.front();
//         q.pop();

//     }
// }

// std::vector<std::pair<vertex_t, vertex_t>> DAG::build(const std::vector<std::pair<vertex_t, vertex_t>> & edges){
//     for(const auto & [u,v] : edges){
//         add_edge(u,v);
//     }
    
//     std::vector<std::pair<vertex_t, vertex_t>> edges;
//     for(const auto & [vertex, _] : succ){

//     }
// }

void DAG::add_edge(vertex_t u, vertex_t v){
    throw_if_not(succ_.at(u).count(v) == 0 && pred_.at(v).count(u) == 0,
                 "edge u -> v already present, u = " + std::to_string(u) + ", v = " + std::to_string(v));
    succ_[u].insert(v);
    pred_[v].insert(u);
}

bool DAG::precedes(vertex_t u, vertex_t v)const{
    return succ_.at(u).count(v) == 1;
}

bool DAG::comparable(vertex_t u, vertex_t v)const{
    return succ_.at(u).count(v) == 1 || succ_.at(v).count(u) == 1;
}

DAG::DAG(const vector<vertex_t> & vertices){
    for(vertex_t v : vertices){
        pred_[v];
        succ_[v];
    }
}
