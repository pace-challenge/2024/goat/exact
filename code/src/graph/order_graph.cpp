#include "order_graph.hpp"
#include <limits>

digraph make_order_graph(const costs& c)
{
    digraph ret;
    for (auto& [v, _]: c.graph.free())
        ret.add_vertex(v);
    
    auto&& X = c.graph.free();
    for (auto i = X.begin(); i != X.end(); ++i)
        for (auto j = std::next(i); j != X.end(); ++j) {
            auto u = i->first;
            auto v = j->first;
            if (c.is_free(u, v))
                continue;
            //at least one is nonzero
            if (c.is_c_zero(u, v))
                ret.add_edge(u, v);
            else if (c.is_c_zero(v, u))
                ret.add_edge(v, u);
            else {
                //both are nonzero, so it is an orientable pair
                auto cuv = c.get_cost(u, v);
                auto cvu = c.get_cost(v, u);
                assert(cuv != -1);
                assert(cvu != -1);
                if (cuv == cvu)
                    continue;
                if (cuv < cvu)
                    ret.add_edge(u, v);
                else
                    ret.add_edge(v, u);
            }
        }
    return ret;
}


weighted_digraph<long long> make_cost_graph(const costs& c)
{
    weighted_digraph<long long> ret;
    for (auto& [v, _]: c.graph.free())
        ret.add_vertex(v);
    auto&& X = c.graph.free();
    for (auto i = X.begin(); i != X.end(); ++i)
        for (auto j = std::next(i); j != X.end(); ++j) {
            auto u = i->first;
            auto v = j->first;
            if (c.is_free(u, v))
                continue;
            //at least one is nonzero
            if (c.is_c_zero(u, v))
                ret.add_weighted_edge(u, v, std::numeric_limits<int>::max());
            else if (c.is_c_zero(v, u))
                ret.add_weighted_edge(v, u, std::numeric_limits<int>::max());
            else {
                auto cuv = c.get_cost(u, v);
                auto cvu = c.get_cost(v, u);
                assert(cuv != -1);
                assert(cvu != -1);
                if (cuv == cvu)
                    continue;
                if (cuv < cvu)
                    ret.add_weighted_edge(u, v, cvu - cuv);
                else
                    ret.add_weighted_edge(v, u, cuv - cvu);
            }
        }
    return ret;
}

