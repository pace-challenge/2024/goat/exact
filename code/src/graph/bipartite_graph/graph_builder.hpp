#pragma once

#include "bip_graph.hpp"


class graph_builder
{
  public:
    /**
     * Sets the fixed partition to be @X
     */
    void set_fixed_part(std::vector<vertex_t>&& part);
    
    /**
     * Sets the free partition to be @Y
     */
    void set_free_part(std::vector<vertex_t>&& part);
    
    /**
     * Clears everything present in this graph -- partitions and edges.
     */
    void reset();
    /**
     * Adds edge @x -- @y to the graph, x is in the fixed part and y is in the free part.
     * Returns true if edge was added (i.e. it was not present before)
     * throws if x or y are invalid (are not in fixed or free respectively)
     */
    bool add_edge(vertex_t x, vertex_t y);
    
    /**
     * Adds edges {y,v} for v \in nb, y must be in free part
     */
    void add_neiy(vertex_t y, const std::vector<vertex_t>& nb);
    /**
     * Adds edges {x,v} for v \in nb, x must be in fixed part
     */
    void add_neix(vertex_t x, const std::vector<vertex_t>& nb);
    
    /**
     * Removes edge @x -- @y from the graph, x is in the fixed part and y is in free part.
     * Returns true if the edge was removed (i.e. it was present before)
     * throws if x or y are invalid (are not in fixed or free respectively)
     */
    bool remove_edge(vertex_t x, vertex_t y);
    
    /**
     * Builds bipartite graph from stored fixed, free parts and edges.
     */
    [[nodiscard]] bip_graph build() const;
  
  private:
    std::vector<vertex_t> fixed_part_;
    std::set<vertex_t> fixed_part_set_;
    std::vector<vertex_t> free_part_;
    std::set<vertex_t> free_part_set_;
    std::set<std::pair<vertex_t, vertex_t>> edges_;
};
