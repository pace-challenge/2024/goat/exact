#pragma once

#include <unordered_set>
#include <unordered_map>
#include <cassert>
#include "../../common.hpp"

class digraph
{
  public:
    bool add_vertex(vertex_t v);
    
    bool add_vertices(auto&& ... vs)
    {
        return (add_vertex(std::forward<decltype(vs)>(vs)) && ...);
    }
    
    bool add_edge(vertex_t from, vertex_t to);
    
    void remove_edge(vertex_t from, vertex_t to);
    
    auto& succ()
    {
        return succ_;
    }
    
    const auto& succ() const
    {
        return succ_;
    }
    
    auto& pred()
    {
        return pred_;
    }
    
    const auto& pred() const
    {
        return pred_;
    }
    
    auto& succ(vertex_t v)
    {
        assert(succ_.contains(v));
        return succ_[v];
    }
    
    const auto& succ(vertex_t v) const
    {
        assert(succ_.contains(v));
        return succ_.at(v);
    }
    
    auto& pred(vertex_t v)
    {
        assert(pred_.contains(v));
        return pred_[v];
    }
    
    const auto& pred(vertex_t v) const
    {
        assert(pred_.contains(v));
        return pred_.at(v);
    }
  
  private:
    std::unordered_map<vertex_t, std::unordered_set<vertex_t>> succ_;
    std::unordered_map<vertex_t, std::unordered_set<vertex_t>> pred_;
};