#pragma once
#include "bipartite_graph/bip_graph.hpp"

struct interval
{
    vertex_t l, r;
    
    bool operator<(const interval& o) const;
};

struct interval_info
{
    static interval_info compute(const bip_graph& graph);
    /**
     * determines if the pair u, v should be ignored (i.e. is free or forced)
     * handles the special case u == v (u == v is ignored)
     */
    bool ignore(vertex_t u, vertex_t v, const bip_graph& g) const;
    
    /*
     * For each vertex y of the free party (Y) the interval (i.e., a pair of vertices from X) that y covers in X
     * In otherwords the leftmost xl and rightmost vertex xr such that xl,xr are neighbors of y
     * */
    std::unordered_map<vertex_t, interval> y_intervals;
    /**
     * For each vertex x of the fixed part (X) the set of vertices from Y such that x is the left endpoint of intervals[y].
     * Note that if x is not a left endpoint of anyone, then x_left_endpoints[x] is an empty set
     */
    std::unordered_map<vertex_t, std::unordered_set<vertex_t>> x_left_endpoints;
    /**
     * For each vertex x of the fixed part (X) the set of vertices from Y such that x is the right endpoint of intervals[y]
     * Note that if x is not a right endpoint of anyone, then x_left_endpoints[x] is an empty set
     */
    std::unordered_map<vertex_t, std::unordered_set<vertex_t>> x_right_endpoints;
};