#pragma once

#include <type_traits>
#include <tuple>

using vertex_t = unsigned int;
static_assert(std::is_integral_v<vertex_t>);

struct edge
{
    vertex_t u, v;
    
    [[nodiscard]] edge reverse() const
    {
        return edge {v, u};
    }
};

constexpr bool operator==(const edge& e, const edge& f)
{
    return e.u == f.u && e.v == f.v;
}

inline constexpr auto edge_cmp_coordinate_wise = [](const edge& e, const edge& f)
{
    return std::tie(e.u, e.v) < std::tie(f.u, f.v);
};

inline constexpr vertex_t INVALID_VERTEX  = (vertex_t)-1;

constexpr vertex_t operator ""_v(unsigned long long int a)
{
    return (vertex_t) a;
}