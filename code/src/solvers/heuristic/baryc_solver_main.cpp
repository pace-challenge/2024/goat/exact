#include "baryc_solver.hpp"
#include "../../graph/bipartite_graph/bip_graph_io.hpp"

int main()
{
    std::cout << baryc_solve_def(load_bip_graph_pace(std::cin));
    return 0;
}