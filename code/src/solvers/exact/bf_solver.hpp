#include <limits>
#include "../../graph/bipartite_graph/bip_graph.hpp"
#include "../solver_result.hpp"
#include "../../evaluator/evaluator.h"


solver_result brute_force_solve(const bip_graph& G)
{
    auto order = G.get_free_part_as<std::vector>();
    std::sort(order.begin(), order.end());
    long long n_crossings = std::numeric_limits<long long>::max();
    std::vector<vertex_t> best;
    while (std::next_permutation(order.begin(), order.end())) {
        long long cur = evaluate(G, {order});
        if (cur < n_crossings) {
            n_crossings = cur;
            best = order;
        }
    }
    return solver_result {.order = best};
}