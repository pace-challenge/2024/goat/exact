#include "heuristic_lowerbound_solver.hpp"
#include "../heuristic/baryc_solver.hpp"
#include "../heuristic/median_solver.hpp"
#include "../../graph/bipartite_graph/bip_graph_io.hpp"

int main()
{
    try {
        std::cout << hl_solve(load_bip_graph_pace(std::cin),median_solve,baryc_solve_def);
    }
    catch(const std::runtime_error&)
    {
        throw;
    }
}