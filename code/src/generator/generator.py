import argparse
import math
import os
import random
from datetime import datetime


def generate_valid_edges_range(x_count, y_count, max_range):
    valid_edges = []

    for y in range(y_count):
        end = random.randint(max_range, x_count)
        for x in range(end - max_range, end):
            valid_edges.append((x, y))
    return valid_edges


def generate_valid_edges_no_range(x_count, y_count):
    valid_edges = []

    for y in range(y_count):
        for x in range(x_count):
            # pridam hranu medzi x a y
            valid_edges.append((x, y))
    return valid_edges


def generate_valid_edges(x_count, y_count, max_range):
    if max_range is not None:
        return generate_valid_edges_range(x_count, y_count, max_range)
    else:
        return generate_valid_edges_no_range(x_count, y_count)


def decision(probability):
    return random.random() < probability

def choose_edges(valid_edges, p):
    chosen_edges = []

    if p > 1:
        p = p / len(valid_edges)

    for edge in valid_edges:
        if decision(p):
            chosen_edges.append(edge)
    return chosen_edges


def print_graph(outpath, x_count, y_count, chosen_edges):
    print('Writing to', outpath)
    f = open(outpath, 'w')
    f.write(str(x_count) + ' ' + str(y_count) + ' ' + str(len(chosen_edges)) + '\n')
    for edge in chosen_edges:
        f.write(str(edge[0]) + ' ' + str(edge[1]) + '\n')
    f.close()


def generate_graphs(outdir, x_count, y_count, max_range, p, graph_count):
    for i in range(graph_count):
        e = generate_valid_edges(x_count, y_count, max_range)
        e = choose_edges(e, p)
        outdir_path = outdir + '/' + str(i) + '_graph.txt'
        print_graph(outdir_path, x_count, y_count, e)


def check_positive(value):
    try:
        value = int(value)
        if value <= 0:
            raise argparse.ArgumentTypeError("{} is not a positive integer".format(value))
    except ValueError:
        raise Exception("{} is not an integer".format(value))
    return value



def check_edge_occurence(x):
    # primarne vyskusaj pravdepodobnost

    # sekundarne vyskusaj ci je to integer

    try:
        x = float(x)
    except ValueError:
        raise argparse.ArgumentTypeError("%r not at least a floating-point literal" % (x,))

    if x >= 0.0 and x <= 1.0:
        return x

    try:
        x = float(x)
    except ValueError:
        raise argparse.ArgumentTypeError("%r note a float between 0 and 1 or an integer" % (x,))

    if x < 1:
        raise argparse.ArgumentTypeError("%r not an integer larger than 1" % (x,))
    return x




def restricted_float(x):
    try:
        x = float(x)
    except ValueError:
        raise argparse.ArgumentTypeError("%r not a floating-point literal" % (x,))

    if x < 0.0 or x > 1.0:
        raise argparse.ArgumentTypeError("%r not in range [0.0, 1.0]"%(x,))
    return x

def readable_dir(prospective_dir):
  if not os.path.isdir(prospective_dir):
    raise argparse.ArgumentTypeError("{0} is not a valid path".format(prospective_dir))
  if os.access(prospective_dir, os.R_OK):
    return prospective_dir
  else:
    raise argparse.ArgumentTypeError("{0} is not a readable dir".format(prospective_dir))



if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('-o', '--output_directory', type=readable_dir,
                        required=True, help='directory to save generated_graphs')
    parser.add_argument('-x', '--x_partite_size', type=check_positive,
                        required=True, help='size of the partite, that will be static and will not be moving')
    parser.add_argument('-y', '--y_partite_size', type=check_positive,
                        required=True, help='size of the partite, that is not static and its order can be changed')
    parser.add_argument('-r', '--range', type=check_positive,
                        required=False, help='the maximum difference between max and min X vertice for each Y vertice')
    parser.add_argument('-e', '--edge_occurence', type=check_edge_occurence,
                        required=True, help='If is between 0.0 and 1.0, is considered a probability for each edge, if it is an integer alrger than 1. it is considered the total edge count in the graph.')
    parser.add_argument('-s', '--seed', type=int,
                        required=False, help='the seed for the pseudorandom generator, if none is specified, is taken from time')
    parser.add_argument('-c', '--graph_count', type=check_positive, default=1,
                        required=False, help='the amount of graphs to be generated')

    args = parser.parse_args()

    if args.seed is not None:
        random.seed(args.seed)
    else:
        random.seed(datetime.now().timestamp())

    generate_graphs(args.output_directory, args.x_partite_size, args.y_partite_size, args.range,
                    args.edge_occurence, args.graph_count)
