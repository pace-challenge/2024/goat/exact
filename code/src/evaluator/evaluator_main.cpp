#include <iostream>
#include <fstream>
#include "evaluator.h"
#include "../graph/bipartite_graph/bip_graph_io.hpp"

void usage(const char* name)
{
    std::cout << "usage: " << name << " <instance> <solution>" << std::endl;
}

solver_result load_sol(std::istream& is)
{
    std::vector<vertex_t> ret;
    vertex_t v;
    while (is >> v)
        ret.push_back(v);
    return {ret};
}

int main(int argc, char* argv[])
{
    if (argc != 3) {
        usage(argv[0]);
        return 0;
    }
    auto inst_file = argv[1];
    auto sol_file = argv[2];
    std::ifstream ifsinst(inst_file);
    std::ifstream ifssol(sol_file);
    if (!ifsinst.is_open()) {
        std::cout << "Could not open " << inst_file << std::endl;
        return 1;
    }
    if (!ifssol.is_open()) {
        std::cout << "Could not open " << sol_file << std::endl;
        return 1;
    }
    
    auto G = load_bip_graph_pace(ifsinst);
    auto sol = load_sol(ifssol);
    std::cout << evaluate(G, sol);
    return 0;
}