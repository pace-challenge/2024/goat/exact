#pragma once

#include <climits>
#include <ranges>
#include "../graph/bipartite_graph/bip_graph.hpp"
#include "../graph/interval_info.hpp"
#include "../graph/costs.hpp"
#include "costs_lb.hpp"
#include "../graph/order_graph.hpp"
#include "../graph/digraph/utilities.hpp"


//determeine if cycle with minimum weight k exists, basically ignore edges with weight less than k
inline bool exists_cycle(const weighted_digraph<long long>& g, std::vector<vertex_t>& cycle, long long k)
{
    std::cerr << "cycle " << k << std::endl;
    std::unordered_map<vertex_t, vertex_t> prev;
    std::unordered_set<vertex_t> stk;
    auto dfs =
            [&](vertex_t v, std::vector<vertex_t>& result, auto&& self) -> bool
            {
                stk.insert(v);
                for (auto& n: g.succ(v)) {
                    if (g.weight(v, n) < k) //skip wedges of weight less than k
                        continue;
                    
                    if (stk.contains(n)) //cycle!
                    {
                        assert(result.empty());
                        do {
                            result.push_back(v);
                            assert(prev.contains(v));
                            v = prev[v];
                        } while (v != n);
                        result.push_back(n);
                        std::reverse(result.begin(), result.end());
                        return true;
                    }
                    if (!prev.contains(n)) {
                        prev[n] = v;
                        if (self(n, result, self))
                            return true;
                        
                    }
                }
                stk.erase(v);
                return false;
            };
    
    std::vector<vertex_t> best;
    for (auto& [v, _]: g.succ()) {
        std::vector<vertex_t> result;
        stk.clear();
        if (!prev.contains(v))
            if (dfs(v, result, dfs) && (best.empty() || result.size() < best.size()))
                best = std::move(result);
    }
    cycle = best;
    return !best.empty();
}

inline bool find_cycle(const weighted_digraph<long long>& g, std::vector<vertex_t>& cycle)
{
    long long idx = -1;
    long long max = 0;
    for (auto& [v, Nv]: g.succ())
        for (auto& w: Nv)
            max = std::max(max, g.weight(v, w));
    std::vector<vertex_t> result;
    std::vector<vertex_t> best;
    for (auto jmp = max / 2; jmp >= 1; jmp >>= 1)
        while (idx + jmp <= max && exists_cycle(g, result, idx + jmp)) {
            best = std::move(result);
            idx += jmp;
        }
    
    if (idx != -1) {
        cycle = best;
        return true;
    } else
        return false;
}


inline long long costs_lower_bound_improved(const costs& cost_structure)
{
    long long basic_cost_lb = costs_lower_bound(cost_structure);
    auto cost_graph = make_cost_graph(cost_structure);
    long long cycles_lb = 0;
    if(!is_acyclic(cost_graph)) {
        std::vector<vertex_t> cycle;
        while (find_cycle(cost_graph, cycle)) {
            long long lightest_edge = INT_MAX;
            for (size_t i = 0; i < cycle.size(); ++i) {
                auto u = cycle[i];
                auto v = cycle[(i + 1) % cycle.size()];
                lightest_edge = std::min(lightest_edge, cost_graph.weight(u, v));
                cost_graph.remove_edge(u, v);
            }
            cycles_lb += lightest_edge;
            for(auto& x : cycle)
                std::cerr << x << " ";
            std::cerr << "\n";
        }
    }
    return basic_cost_lb + cycles_lb;
}

inline long long costs_lower_bound_improved(const bip_graph& g)
{
    return costs_lower_bound_improved(costs::compute(g, interval_info::compute(g)));
}