#pragma once

#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <cassert>
#include <algorithm>
#include <stdexcept>
#include <list>

/**
 * @brief Checks that container contains unique elements
 */
template <typename Cont>
bool is_unique(const Cont& container)
{
    std::unordered_set<typename Cont::value_type> elements;
    for (auto&& x: container)
        if (!elements.insert(x).second)
            return false;
    return true;
}

/**
 * @brief Creates positional map from order_.
 * @example order_ = {1,5,7,3,2,6}
 *          =>
 *          return = {1->0, 5->1, 7->2, 3->3, 2->4, 6->5}
 */
template <typename T>
std::unordered_map<T, size_t> to_pos(const std::vector<T>& order)
{
    std::unordered_map<T, size_t> pos;
    for (size_t i = 0; i < order.size(); ++i)
        pos[order[i]] = i;
    return pos;
}
/**
 * Convers positional map to order.
 * @example pos = {1-> 0, 5-> 1, 7-> 2, 3-> 3, 2-> 4, 6-> 5}
 *          =>
 *          return = {1,5,7,3,2,6}
 *
 *          pos = {1->"a", 5->"b",7->"c", 3->"d", 2->"e", 6->"f"}
 *          =>
 *          return = {1,5,7,3,2,6}
 * If any values in the map compare equal, then order in the result is unspecified.
 */
template <typename T, typename O>
std::vector<T> to_ord(const std::unordered_map<T, O>& pos)
{
    std::vector<T> order;
    std::vector<std::pair<O, T>> order_idx;
    for (auto& [val, ord]: pos)
        order_idx.emplace_back(ord, val);
    std::sort(order_idx.begin(), order_idx.end());
    for (auto& [ord, val]: order_idx)
        order.push_back(val);
    return order;
}

/**
 * Checks if two containers contain the same elements, up to permutation.
 */
template <typename Cont1, typename Cont2>
bool is_same_up_to_permutation(const Cont1& a, const Cont2& b)
{
    if(std::size(a) != std::size(b))
        return false;
    using T = std::common_type_t<typename Cont1::value_type, typename Cont2::value_type>;
    std::vector<T> a_(a.begin(), a.end());
    std::vector<T> b_(b.begin(), b.end());
    std::sort(a_.begin(), a_.end());
    std::sort(b_.begin(), b_.end());
    return a_ == b_;
}


template<typename Cont1, typename Cont2>
bool is_rotation_of(const Cont1& a, const Cont2& b)
{
    if(std::size(a) != std::size(b))
        return false;
    using T = std::common_type_t<typename Cont1::value_type, typename Cont2::value_type>;
    std::list<T> a_(a.begin(),a.end());
    std::list<T> b_(b.begin(),b.end());
    for(size_t i = 0; i < a_.size(); ++i)
    {
        if(a_ == b_)
            return true;
        a_.push_back(std::move(a_.front()));
        a_.pop_front();
    }
    
    return false;
}

template <typename Exc = std::runtime_error>
void throw_if_not(bool cond, auto&& ...excargs)
{
    if (!cond)
        throw Exc(std::forward<decltype(excargs)>(excargs)...);
}

#define THROW_IF_NOT(expr, errmsg) throw_if_not(expr, "Expression " #expr " failed: " + std::string(errmsg))


