#pragma once

#include <iterator>
#include <sstream>
#include <ranges>
#include "../graph/bipartite_graph/bip_graph.hpp"
#include "../graph/costs.hpp"


void RR2(const bip_graph &G, costs &c)
{
    // keys:   fixed part neighbors represented as string
    // values: free part vertices with neighbors given by the corresponding key
    std::unordered_map<std::string, std::unordered_set<vertex_t>> common_neighborhood;

    // find vertices with common neighborhood
    for (auto&& item : G.free())
    {
        std::set<vertex_t> ordered(item.second.begin(), item.second.end());
        std::ostringstream stream;
        std::copy(ordered.begin(), ordered.end(), std::ostream_iterator<vertex_t>(stream, ","));
        std::string key = stream.str();

        auto it = common_neighborhood.find(key);
        if (it == common_neighborhood.end())
        {
            common_neighborhood[key] = {item.first};
        }
        else
        {
            (*it).second.insert(item.first);
        }
    }

    // use it to update costs structure
    for (auto&& item : common_neighborhood)
    {
        long n_twins = (long)item.second.size();

        if (n_twins < 2)
            continue;

        // pick one of the twins to be kept
        vertex_t chosen = *(item.second.begin());

        for (auto&& vertex : G.get_free_part_as<std::vector>())
        {
            // update costs between every other free vertex and the chosen one
            if (c.get_cost(chosen, vertex) != -1)
                c.c[std::make_pair(chosen, vertex)] *= n_twins;
            if (c.get_cost(vertex, chosen) != -1)
                c.c[std::make_pair(vertex, chosen)] *= n_twins;

            // delete all the twins except the chosen one from costs
            for (auto&& twin : item.second | std::views::drop(1))
            {
                if (c.get_cost(vertex, twin) != -1)
                    c.c.erase(std::make_pair(vertex, twin));
                if (c.get_cost(twin, vertex) != -1)
                    c.c.erase(std::make_pair(twin, vertex));
            }
        }
    }
}
