## Getting Started

Using `cmake`. To compile release version:

```
mkdir build-release
cd build-release
cmake -DCMAKE_BUILD_TYPE=Release
cmake ..
make
```

`make` can be optionally run with `-j X` where `X` is the number of parallel threads used.

After build, the solver `hl_solver` should be found in the `build-release/src` folder.
